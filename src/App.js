import React, { useState } from 'react';
import './App.css';
import { Grid } from '@material-ui/core';
import RequestSender from './RequestSender';
import { MailManAppBar, RequestsDrawer, RequestSendingBar } from './components';
import ReactJson from 'react-json-view'
import CircularProgress from '@material-ui/core/CircularProgress';
import _ from "lodash";

const getRequestsFromLocalStorage = () => {
    const requests = localStorage.getItem('savedRequests');
    if (_.isEmpty(requests)) {
        const initialStorageState = {
            saved: []
        }
        localStorage.setItem('savedRequests', JSON.stringify(initialStorageState))
    }
    return localStorage.getItem('savedRequests');
}

const App = () => {
    const [httpMethod, setHttpMethod] = useState('GET');
    const [address, setAddress] = useState('');
    const [requests, setRequests] = useState(JSON.parse(getRequestsFromLocalStorage()));
    const [requestResult, setRequestResult] = useState({});
    const [requestLoading, setRequestLoading] = useState(false);
    const [requestName, setRequestName] = useState('');
    let headers = [
        {
            key: '',
            value: '',
            description: '',
        },
        {
            key: '',
            value: '',
            description: '',
        },
        {
            key: '',
            value: '',
            description: '',
        },
        {
            key: '',
            value: '',
            description: '',
        },
        {
            key: '',
            value: '',
            description: '',
        }
    ];
    let body = {};

    const removeRequest = (index) => {
        const requests = JSON.parse(getRequestsFromLocalStorage());
        const reducedRequests = _.cloneDeep(requests);
        reducedRequests.saved.splice(index, 1);
        localStorage.setItem('savedRequests', JSON.stringify(reducedRequests))
        setRequests(JSON.parse(getRequestsFromLocalStorage()))
    }

    const saveRequest = () => {
        const reqToSave = {
            target: address,
            name: requestName,
            requestType: httpMethod,
            body: body,
            headers: headers.filter((header) => !_.isEmpty(header.key) && !_.isEmpty(header.value))
        }
        const requests = JSON.parse(getRequestsFromLocalStorage());
        requests.saved.push(reqToSave);

        localStorage.setItem('savedRequests', JSON.stringify(requests))
        setRequests(JSON.parse(getRequestsFromLocalStorage()))
    }

    const loadSavedRequest = (req) => {
        setAddress(req.target);
        setHttpMethod(req.requestType);
        body = _.cloneDeep(req.body);
        headers = _.cloneDeep(req.headers);
    }

    const onHttpMethodTypeChange = (event) => {
        setHttpMethod(event.target.value);
    };

    const sendRequest = async (headers = [], body = {}) => {
        const headersToSend = headers.filter((header) => !_.isEmpty(header.key) && !_.isEmpty(header.value))
        setRequestLoading(true);
        try {
            const result = await RequestSender.sendRequest(address, httpMethod, body, headersToSend);
            setRequestResult(result.data);
        }
        catch (err) {
            setRequestResult({err});
        }
        setRequestLoading(false);
    };

    const setRows = (newRows) => {
        headers = _.cloneDeep(newRows);
    };

    const setBody = (newBody) => {
        body = _.cloneDeep(newBody);
    };

  return (
      <>
        <MailManAppBar/>
        <Grid container spacing={2}>
            <Grid item xs={10}>
                <RequestSendingBar
                   setBody={(newBody) => setBody(newBody)}
                   setRows={(newRows) => setRows(newRows)}
                   rows={headers}
                   body={body}
                   address={address}
                   httpMethod={httpMethod}
                   onHttpMethodTypeChange={(event) => onHttpMethodTypeChange(event)}
                   sendRequest={() => sendRequest(headers, body)}
                   setAddress={(address) => setAddress(address)}
                   saveReq={() => saveRequest()}
                   requestName={requestName}
                   setRequestName={(reqName) => setRequestName(reqName)}
                />
            </Grid>
            <Grid item xs={5}>
              <RequestsDrawer requests={requests} loadSavedRequest={loadSavedRequest} removeRequest={(req) => removeRequest(req)}/>
            </Grid>
            <Grid item xs={5}>
                {
                    requestLoading ? (<CircularProgress style={{ padding: 150 }} color="secondary" />)
                        : (<ReactJson displayObjectSize={true} name={"result"} collapsed={5} src={requestResult} />)
                }
            </Grid>
        </Grid>
      </>
  );
};

export default App;
