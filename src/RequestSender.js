import axios from 'axios';

const DEFAULT_HEADERS = {
    'Accept': 'application/json',
    'Content-Type': 'application/json;charset=UTF-8'
};

class RequestSender {

    static createHeaders = (headers) => {
        const retValue = {};
        headers.forEach(header => {
            retValue[header.key] = header.value;
        });
        return retValue;
    }

    static async sendRequest(to, method, data = {}, headers) {
        const options = {
            url: to,
            method: method,
            headers: headers ? this.createHeaders(headers) : DEFAULT_HEADERS,
            data
        };

        return axios(options)
            .then(response => {
                return response;
            });
    }
}

export default RequestSender;
