import { TextareaAutosize } from "@material-ui/core";
import React from "react";
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles({
    textarea: {
        width: '100%',
    },
});

export const RequestResultArea = (props) => {
    const classes = useStyles();

    return (
            <TextareaAutosize
                readOnly
                rowsMin={20}
                rowsMax={40}
                aria-label="result_area"
                placeholder="Send a request to get a result here."
                defaultValue={props.defaultValue}
                className={classes.textarea}
            />
    );
};
