import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Modal from '@material-ui/core/Modal';
import _ from "lodash";
import Backdrop from "@material-ui/core/Backdrop";
import {JSONEditor} from 'react-json-editor-viewer';
import Fade from '@material-ui/core/Fade';
import {Button} from "@material-ui/core";
import AddIcon from '@material-ui/icons/Add';
import TextField from "@material-ui/core/TextField";
import Popover from "@material-ui/core/Popover";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles((theme) => ({
    root: {
        marginLeft: 20,
        marginTop: 20,
    },
    modal: {
        minWidth: 400,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        border: '2px solid #000',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
    },
    button: {
        marginTop: 20,
        marginRight: '90%',
        paddingTop: 5
    },
    table: {
        minWidth: 450,
    },
    keyField: {
        marginRight: 20,
    },
    buttonWrapper: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent:'right',
        marginRight: '10%'
    }
}));

export const RequestBodyModal = (props) => {
    const classes = useStyles();
    const clonedBody = _.cloneDeep(props.body);
    const [data, setData] = useState(clonedBody);
    const [error, setError] = useState(undefined);
    const [anchorEl, setAnchorEl] = useState(null);
    const open = Boolean(anchorEl);
    const id = open ? 'error-popover' : undefined;
    const [newKey, setNewKey] = useState('');
    const [newValue, setNewValue] = useState('');

    const onJsonChange = (key, value, parent, data) => {
        console.log(key, value, parent, data);
    }

    const handleClose = () => {
        setAnchorEl(null);
    };

    return (
        <>
            <Modal
                className={classes.modal}
                open={props.bodyModalOpen}
                onClose={() => props.setBodyModalOpen(false)}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                    timeout: 500,
                }}
            >
                <Fade in={props.bodyModalOpen}>
                  <div className={classes.paper}>
                      <h2 id="request-body-modal-title">Request Body</h2>
                          <JSONEditor
                              data={data}
                              collapsible
                              onChange={onJsonChange}
                          />
                      <TextField classes={classes.keyField} id="new-entry-key" label="Key" value={newKey} onChange={(e) => setNewKey(e.target.value)}/>
                      <TextField id="new-entry-value" label="Value" value={newValue} onChange={(e) => setNewValue(e.target.value)}/>

                      <div className={classes.buttonWrapper}>
                              <AddIcon color="primary" onClick={(event) => {
                                  setAnchorEl(event.currentTarget);
                                  if (!_.isEmpty(newKey) && !_.isEmpty(newValue)) {
                                      const newData = _.cloneDeep(data);
                                      if (!Object.keys(data).includes(newKey)) {
                                          newData[newKey] = newValue;
                                          setData(newData);
                                          setError(undefined);
                                      } else {
                                          setError('Object keys mut be unique!');
                                      }
                                  } else {
                                      setError('Key and value must be a valid string!');
                                  }
                              }} style={{marginTop: 20, marginLeft: '90%', fontSize: 25, paddingTop: 5 }} />
                              <Button className={classes.button} onClick={() => {
                                  props.saveBody(data);
                                  props.setBodyModalOpen(false);
                              }} variant="contained" color="primary">
                                  Save
                              </Button>
                      </div>
                  </div>
                </Fade>
            </Modal>
            {
                !_.isEmpty(error) && <Popover
                    id={id}
                    open={open}
                    anchorEl={anchorEl}
                    onClose={handleClose}
                    anchorOrigin={{
                        vertical: 'bottom',
                        horizontal: 'center',
                    }}
                    transformOrigin={{
                        vertical: 'top',
                        horizontal: 'center',
                    }}
                >
                    <Typography className={classes.typography}>{error}</Typography>
                </Popover>
            }
        </>
    )
}
