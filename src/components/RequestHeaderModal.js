import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Tooltip from "@material-ui/core/Tooltip";
import TextField from "@material-ui/core/TextField";
import { Button } from "@material-ui/core";
import _ from "lodash";

const useStyles = makeStyles((theme) => ({
    root: {
        marginLeft: 20,
        marginTop: 20,
    },
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        border: '2px solid #000',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
    },
    button: {
        marginTop: 20,
        marginLeft: '90%',
        paddingTop: 5,
    },
    table: {
        minWidth: 450,
    },
}));

export const RequestHeaderModal = (props) => {
    const classes = useStyles();
    const localRows = _.cloneDeep(props.rows);

    return (<Modal
        className={classes.modal}
        open={props.headerModalOpen}
        onClose={() => props.setHeaderModalOpen(false)}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
            timeout: 500,
        }}
    >
        <Fade in={props.headerModalOpen}>
            <div className={classes.paper}>
                <h2 id="request-header-modal-title">Headers</h2>
                <TableContainer component={Paper}>
                    <Table className={classes.table} aria-label="headers-table">
                        <TableHead>
                            <TableRow>
                                <TableCell>Key</TableCell>
                                <TableCell align="right">Value</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {localRows.map((row, i) => (
                                <TableRow key={row.key + " " + i}>
                                    <TableCell component="th" scope="row">
                                        <TextField
                                            id={"key-" + i}
                                            label="Key"
                                            variant="outlined"
                                            onChange={(e) => {
                                                localRows[i].key = e.target.value;
                                            }}/>
                                    </TableCell>
                                    <TableCell align="center">
                                        <TextField
                                            id={"value-" + i}
                                            label="Value"
                                            variant="outlined"
                                            onChange={(e) => localRows[i].value = e.target.value}/>
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
                <Tooltip title="Add these headers to the request">
                    <Button className={classes.button} onClick={() => {
                        props.saveHeaders(localRows)
                        props.setHeaderModalOpen(false)
                    }} variant="contained" color="secondary">
                        Save
                    </Button>
                </Tooltip>
            </div>
        </Fade>
    </Modal>)
};
