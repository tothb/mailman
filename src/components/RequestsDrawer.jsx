import React, {useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import DeleteRoundedIcon from '@material-ui/icons/DeleteRounded';
import Box from '@material-ui/core/Box';
import Tooltip from '@material-ui/core/Tooltip';

const useStyles = makeStyles({
    root: {
        marginLeft: 20,
        width: '50%',
        height: 400
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    title: {
        fontSize: 20,
    },
    pos: {
        marginBottom: 12,
    },
    deleteIcon: {
        marginBottom: 12,

    }
});

export const RequestsDrawer = (props) => {
    const classes = useStyles();
    const bullet = <span className={classes.bullet}>•</span>;

    return (
        <Card className={classes.root} variant="outlined">
            <CardContent>
                <Typography className={classes.title} color="textSecondary" gutterBottom>
                    {bullet}Saved requests{bullet}
                </Typography>
                <>
                {
                    props.requests.saved.map((req, i) => (
                            <>
                                <Box borderColor="grey.500" borderRadius={8} border={0.2} style={{ marginBottom: 10 }}>
                                    <Tooltip title={"Request to: " + req.target}>
                                        <Typography onClick={() => props.loadSavedRequest(req)} variant="body1" component="p">
                                            {req.name} {bullet} {req.requestType}
                                        </Typography>
                                    </Tooltip>
                                    <Tooltip title="Delete">
                                        <DeleteRoundedIcon color="primary" onClick={() => props.removeRequest(i)} style={{marginLeft: '90%', fontSize: 25, paddingTop: 5 }} />
                                    </Tooltip>
                                </Box>
                            </>
                        )
                    )
                }
                </>
            </CardContent>

        </Card>
    );
};
