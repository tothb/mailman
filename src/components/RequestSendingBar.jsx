import React, { useState } from "react";
import Tooltip from '@material-ui/core/Tooltip';
import { Button, MenuItem, Select, TextField } from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";
import { RequestHeaderModal } from "./RequestHeaderModal";
import { RequestBodyModal } from "./RequestBodyModal";

const useStyles = makeStyles((theme) => ({
    root: {
        marginLeft: 20,
        marginTop: 20,
    },
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        border: '2px solid #000',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
    },
    button: {
      marginRight: 10,
    },
    saveButton: {
        marginTop: 15
    },
    saveWrapper: {
        marginTop: 20
    }
}));

const HTTP_METHODS_TO_RENDER = [
    'GET',
    'POST',
    'PUT',
    'UPDATE',
    'DELETE',
];

export const RequestSendingBar = (props) => {
    const classes = useStyles();
    const [headerModalOpen, setHeaderModalOpen] = useState(false);
    const [bodyModalOpen, setBodyModalOpen] = useState(false);

    return (
        <Grid className={classes.root} display="flex" flexDirection="row" p={1} component="span" m={1} container spacing={2}>
            <Grid item xs={10}>
                <Tooltip title="Choose an http method" arrow>
                    <Select
                        labelId="demo-simple-select-label"
                        id="http-method-select"
                        value={props.httpMethod}
                        onChange={props.onHttpMethodTypeChange}
                    >
                        {
                            HTTP_METHODS_TO_RENDER.map(method => {
                                return <MenuItem value={method}>{method}</MenuItem>
                            })
                        }
                    </Select>
                </Tooltip>
            </Grid>
            <Grid item xs={10}>
                {
                    headerModalOpen && (
                        <RequestHeaderModal saveHeaders={(newRows) => props.setRows(newRows)}
                                            rows={props.rows}
                                            headerModalOpen={headerModalOpen}
                                            setHeaderModalOpen={(newValue) => setHeaderModalOpen(newValue)}/>
                    )
                }
                {
                    bodyModalOpen && (
                        <RequestBodyModal   saveBody={(newBody) => props.setBody(newBody)}
                                            body={props.body}
                                            bodyModalOpen={bodyModalOpen}
                                            setBodyModalOpen={(newValue) => setBodyModalOpen(newValue)}/>
                    )
                }

                <TextField fullWidth id="address_input" label="Enter request URL" value={props.address} onChange={(e) => props.setAddress(e.target.value)} />
            </Grid>
            <Grid item xs={10}>
                <Button className={classes.button} onClick={() => setHeaderModalOpen(true)} variant="contained" color="primary">
                    Headers
                </Button>
                <Button className={classes.button} onClick={() => setBodyModalOpen(true)} variant="contained" color="primary">
                    Body
                </Button>
                <Button disabled={props.address === ''} className={classes.button} onClick={() => props.sendRequest()} variant="contained" color="secondary">
                    SEND
                </Button>
                <div className={classes.saveWrapper}>
                    <TextField fullWidth id="request_name" label="Enter request name" value={props.requestName} onChange={(e) => props.setRequestName(e.target.value)} />
                    <Button disabled={props.requestName === ''} className={classes.saveButton} onClick={() => props.saveReq()} variant="contained" color="secondary">
                        SAVE
                    </Button>
                </div>
            </Grid>
        </Grid>
    );
};
