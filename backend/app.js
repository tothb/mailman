const express = require('express');
const { Sequelize, DataTypes } = require('sequelize');
const bodyParser = require('body-parser');

const app = express();
const jsonParser = bodyParser.json();
const sequelize = new Sequelize('sqlite::memory:');

const Request = sequelize.define('Request', {
    // Model attributes are defined here
    target: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
    },
    requestType: {
        type: DataTypes.STRING,
        allowNull: false,
    }
}, {}
);

app.get('/requests', async (req, res) => {
    try {
        await sequelize.authenticate();
        res.send("Hello world3!");
    } catch (error) {
        console.error('Unable to connect to the database:', error);
    }
});

app.post('/init', async (req, res) => {
    await sequelize.sync({ force: true });
});

app.post('/create', jsonParser, async (req, res) => {
//TODO
    const request = Request.build(
    {
        target: req.body.target,
        requestType: req.body.requestType
    });

    const savedRequest = await request.save();
    res.send(savedRequest);

});

app.listen(4000);
