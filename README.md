## Vállalati információs rendszerek - IMN252L-1

Készítette: Tóth Botond (J8TKXW)

### Indítás

- yarn install
- yarn start
- yarn electron-start

### Felhasznált technológiák

- Javascript
- [Electron](https://www.electronjs.org/)
- [React](https://en.reactjs.org/)
