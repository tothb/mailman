const { app, BrowserWindow } = require('electron');

createWindow = () => {
    const win = new BrowserWindow({ width: 1024, height: 768, resizable: false });
    win.loadURL('http://localhost:3000/')
};

app.on('ready', createWindow);

